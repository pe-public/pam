# Changelog

## [v1.3] (https://code.stanford.edu/pe-public/pam/-/tree/v1.3) (2024-02-02)

* Set authentication principal for duo_config to host/fqdn, otherwise multiple
  principals in a host keytab may lead to a failure to aquire duo keys from
  wallet.

## [v1.2] (https://code.stanford.edu/pe-public/pam/-/tree/v1.2) (2023-06-29)

* Added support for EL9.
* Added a parameter for automatic home directory creation.

## [v1.1] (https://code.stanford.edu/pe-public/pam/-/tree/v1.1) (2021-08-30)

* Fix a bug in Debian templates for pure pubkey authentication.

## [v1.0] (https://code.stanford.edu/pe-public/pam/-/tree/v1.0) (2020-10-26)

* Initial release
