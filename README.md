PAM Puppet Module
=================

The module controls PAM configuration. 

## Dependencies

* `ssh` - the module heavily relies on ssh module and would not produce any working configurations without it.
* `duo` - if DUO second factor authentication is enabled, this module must be handling the installation of the package and providing duo_config resource.
* `su-kerberos` - if GSSAPI is enabled, this module is required to handle the kerberos client configuration.
* `afs` - to support AFS tokens, the AFS has to be deployed. Note that pam_afs_session PAM module is required for the module to handle Kerberos passwords and tickets no matter if AFS is desired or not.

## Parameters

Module has a number of parameters allowing to create a variety of PAM configurations. 

### Authentication configuration

This is a set or parameters defining which authentication methods to use. All these paramters must be in sync with the identically named parameters in `ssh` module to produce the working configurations.

##### enable_duo

Enable DUO support in PAM. 

##### gssapi

Enable GSSAPI authentication. 

##### pubkey

Enables ssh public key authentication. 

#####

### DUO parameters

PAM module uses duo_config resource from `duo` module to create custom DUO configurations for different user types (see `ssh` module documentation for further details). These are the parameters passed along to this resource:

##### failmode

Can be set to either `safe` or `secure`. If a DUO PAM module fails for some reason (misconfiguration or failure to connect to the cloud service), in `safe` mode it fails open allowing logins based on the primary authentication results, while in `secure` mode the authentication fails completely.

##### http_proxy

Set a proxy for DUO authentication if the server does not have direct connection to the interent. The proxy must be given in a format supported by DUO: `http(s)://proxy.host.com:port`. 

##### autopush

DUO immediately sends a push authentication request without giving a user a choice of a second factor device. Note that this options currently triggers a bug in this version of a module and at the moment is ignored.

##### duo_config_dir

A directory where DUO should look for configuration files. Default is a debian-style `/etc/security`.

##### native_duo_package

Debian distributes DUO packages in OS repos. If true, it is assumed that DUO has been installed using such package. If false, the module assumes that DUO has been installed from a package provided by DUO from their own repos. The difference is the location of the PAM module library in the file system.

##### duo_secrets

By default the module gets DUO configuration files from wallet. If wallet is unavailable, a file path can be given as a source. File must contain the same information as available in wallet. Based on this source file `duo_config` resource would create custom configurations for different types of users.

### Other parameters

##### custom_templates

There are always one-offs and this parameter allows to specify custom PAM templates for any managed configuration file. The format of the parameter is a hash having file names as keys and puppet template paths as values:

```yaml
pam::custom_templates:
  ssh: 'profiles/radius/etc/pam.d/sshd.erb'
  common-session: 'profiles/radius/etc/pam.d/common-sesson.erb'
```
