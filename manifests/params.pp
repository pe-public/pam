# defaults for duo packages
#
class pam::params {

  $managed_pam_files = {
    'Debian' => [ 'common-auth', 'common-account', 'common-session',
                  'common-password', 'sshd' ],
    'RedHat' => [ 'sshd' ],
  }

  # special groups
  $auth_groups = {
    password_no_duo  => 'passwd_noduo',
    pubkey_no_duo   => 'pubkey_noduo',
    pubkey_with_duo => 'pubkey_duo',
    gssapi_no_duo   => 'gssapi_noduo'
  }

}
