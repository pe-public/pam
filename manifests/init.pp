# @summary
#   Sets up basic PAM configuration, separated out from the original kerberos
#   configuration, but still dependent on it.
#
#
# @param enable_duo 
#   If true, DUO authentication is enabled. Must be in sync with
#   the same parameter in ssh module to work properly.
#
# @param gssapi
#   If true, GSSAPI authentication support is included.
#
# @param pubkey
#   If true, public key authentication support is included.
#
# @param failmode
#   Sets fail mode for duo. Read full description in duo module
#   documentation.
#
# @param http_proxy 
#   Set a proxy for the servers not having access to the internet
#   in a format used by duo. See duo module documentation for
#   further details.
#
# @param autopush
#   Sets automatic push of duo prompt to users phone without any
#   prompt in the terminal. IMPORTANT this triggers a bug in the
#   current version of the module and is presently ignored.
#
# @param duo_config_dir
#   Directory to store duo configuration files.
#
# @param native_duo_package
#   If true, the duo PAM module installed by a package from an
#   OS repo is used. Otherwise a module installed from DUO's own
#   packages. Should be in sync with the same parameter in DUO
#   module.
#
# @param duo_secrets
#   The souce of the duo configuration information. If undefined,
#   the code acquires it directly from wallet. Alternatively a file
#   can be provided with the same information and the actual files
#   used by a PAM moudle would be derived from it. Useful for 
#   the systems that do not have direct access to wallet, like the
#   ones in the cloud deployments.
#
# @param custom_templates
#   Custom PAM ssh service templates. There are always one-offs.
#   The parameter is a hash where keys are PAM file names and
#   values are absolute paths to custom templates.
#
# @param autocreate_homedirs
#   If true, home directories for authenticated users would be
#   create automatically if they do not exist.
#
# @param auth_groups
#   A hash of names for special workgroups giving user a
#   particular way to authenticate.
#
class pam (
  Boolean $enable_duo = true,
  Boolean $gssapi = true,
  Boolean $pubkey = true,
  Enum['safe','secure'] $failmode = 'safe',
  Optional[String] $http_proxy = undef,
  Optional[Boolean] $autopush = undef,
  Stdlib::Absolutepath  $duo_config_dir = '/etc/security',
  Boolean $native_duo_package = false,
  Optional[Stdlib::Absolutepath] $duo_secrets = undef,
  Optional[Hash[String, String]] $custom_templates = undef,
  Boolean $autocreate_homedirs = false,
  Hash[String,String] $auth_groups = {},
){
  # os-dependent information
  include pam::params

  # override default authentication group names if needed.
  $auth_groups_real = $::pam::params::auth_groups + $auth_groups

  # set the name for duo library
  case $facts['os']['family'] {
    'Debian': {
      $duo_library = $native_duo_package ? {
        true  => 'pam_duo.so',
        false => '/lib64/security/pam_duo.so'
      }
    }
    'RedHat': {
      $duo_library = '/lib64/security/pam_duo.so'
    }
    default: {
      fail('OS family not supported.')
    }
  }

  $duo_rsrc = $enable_duo ? {
    true    => 'present',
    default => 'absent',
  }

  # duo configuration for regular users
  duo_config { "${duo_config_dir}/pam_duo_${::hostname}_sunet.conf":
    ensure         => $duo_rsrc,
    auth_principal => "host/${facts['networking']['fqdn']}",
    source         => $duo_secrets,
    send_gecos     => false,
    failmode       => $failmode,
    autopush       => false,
    http_proxy     => $http_proxy,
  }

  # duo configuration for admins
  duo_config { "${duo_config_dir}/pam_duo_${::hostname}_root.conf":
    ensure         => $duo_rsrc,
    auth_principal => "host/${facts['networking']['fqdn']}",
    source         => $duo_secrets,
    send_gecos     => true,
    failmode       => $failmode,
    autopush       => false,
    http_proxy     => $http_proxy,
  }

  $pam::params::managed_pam_files[$facts['os']['family']].each |$pam_file| {
    file { "/etc/pam.d/${pam_file}":
      content => ($pam_file in $custom_templates) ? {
        true  => template($custom_templates[$pam_file]),
        false => template("pam/etc/pam.d/${facts['os']['family']}/${pam_file}.erb")
      }
    }
  }
}
